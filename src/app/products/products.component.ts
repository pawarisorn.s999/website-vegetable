import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  items = [
    {before:"$3.99",sale_price:'$2',type:"Vegetables",name:"Redish 500g",bg:"bg-1",img:'../../assets/body/Rectangle 19-4.png'},
    {before:"$1.99",sale_price:'$1',type:"Vegetables",name:"Potatos 1kg",bg:"bg-2",img:'../../assets/body/Rectangle 19-5.png'},
    {before:"$0.99",sale_price:'$0.30',type:"Fruits",name:"Tomatos 200g",bg:"bg-3",img:'../../assets/body/Rectangle 19-6.png'},
    {before:"$2.99",sale_price:'$1.50',type:"Vegetables",name:"Broccoli 1kg",bg:"bg-4",img:'../../assets/body/Rectangle 19-7.png'},
    {before:"$1.99",sale_price:'$1',type:"Vegetables",name:"Green Beans 250g",bg:"bg-5",img:'../../assets/body/Rectangle 19-8.png'},
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
