import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {


  items = [
    {
      title:"Top Sells",
      content:[
        {img:"../../assets/body/image 7.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 4.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 5.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"}
      ]
    },
    {
      title:"Top Rated",
      content:[
        {img:"../../assets/body/image 3.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 4.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 6.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"}
      ]
    },
    {
      title:"Trending Items",
      content:[
        {img:"../../assets/body/image 9.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 8.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 7.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"}
      ]
    },
    {
      title:"Recently Added",
      content:[
        {img:"../../assets/body/image 6.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 9.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"},
        {img:"../../assets/body/image 5.png",sub_title:"Orange 1kg",before:"$3.99",price:"$2"}
      ]
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
