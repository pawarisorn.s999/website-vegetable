import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  searchForm = new FormGroup({
    input: new FormControl()
  })
  constructor() { }

  ngOnInit(): void {
  }

  Submit(){
    console.log('submit');
  }

}
