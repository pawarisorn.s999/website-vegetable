import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-multiple-slide',
  templateUrl: './multiple-slide.component.html',
  styleUrls: ['./multiple-slide.component.scss']
})
export class MultipleSlideComponent implements OnInit {

  items = [
    {text:"20 items",bg:"bg-1",name:"Peach",img:'../../assets/body/image 3.png'},
    {text:"220 items",bg:"bg-2",name:"Vegetables",img:'../../assets/body/image 4.png'},
    {text:"10 items",bg:"bg-3",name:"strawberry",img:'../../assets/body/image 5.png'},
    {text:"40 items",bg:"bg-4",name:"Apple",img:'../../assets/body/image 6.png'},
    {text:"23 items",bg:"bg-5",name:"Orange",img:'../../assets/body/image 7.png'},
    {text:"3 items",bg:"bg-6",name:"Potato",img:'../../assets/body/image 8.png'},
    {text:"9 items",bg:"bg-7",name:"Carrot",img:'../../assets/body/image 9.png'}
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
