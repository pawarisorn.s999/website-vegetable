import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DowloadAppComponent } from './dowload-app.component';

describe('DowloadAppComponent', () => {
  let component: DowloadAppComponent;
  let fixture: ComponentFixture<DowloadAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DowloadAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DowloadAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
