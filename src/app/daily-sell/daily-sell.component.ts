import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-daily-sell',
  templateUrl: './daily-sell.component.html',
  styleUrls: ['./daily-sell.component.scss']
})
export class DailySellComponent implements OnInit {

  dataForm = new FormGroup({

  })

  items = [
    {pro:"Save 10%",unit:"20/50",progress:"w-75",before:"$25",sale_price:'$20',type:"Coffe & teas",name:"Coffe 1kg",bg:"bg-1",img:'../../assets/body/Rectangle 19-4.png'},
    {pro:"Best deal",unit:"7/20",progress:"w-50",before:"$10",sale_price:'$4',type:"Meat",name:"Halal Sausage 350g",bg:"bg-2",img:'../../assets/body/Rectangle 19-5.png'},
    {pro:"Save 4%",unit:"32/50",progress:"w-75",before:"$7",sale_price:'$3',type:"Coffe & Teas",name:"Green Tea 250g",bg:"bg-3",img:'../../assets/body/Rectangle 19-6.png'},
    {pro:"Save 8%",unit:"2/10",progress:"w-25",before:"$2",sale_price:'$0.55',type:"Vegetables",name:"Onion 1kg",bg:"bg-4",img:'../../assets/body/Rectangle 19-7.png'},
  ]
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){

  }

}
